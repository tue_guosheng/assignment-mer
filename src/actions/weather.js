
// Load User

import axios from 'axios';

export const getWeather=async (formData)=> {
    const {city, country } =formData
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&?units=metric&APPID=325f17624fb55affa459e364080599df`;
    return new Promise((resolve, reject) => {
        axios.get(url).then(res=>{
            resolve(res)
        }
            
        )  .catch(error => {
            if (error) return reject(error);
            
        });
    })
    
  }

  