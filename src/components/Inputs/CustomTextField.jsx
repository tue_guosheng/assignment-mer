import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    underline: {
        "&&&:before": {
            borderBottom: "none"
        },
        "&&:after": {
            borderBottom: "none"
        }
    },
   
    label:{
      marginLeft:15,
      fontSize: 28,
      fontWeight:400,
      color: "#000",
      "&$labelFocused": {
        color: "#000"
      }
    },
    inputRoot: {
        fontSize: 16
      },
      labelRoot: {
        fontWeight:500,
        paddingLeft:25,
        opacity:'70%',
        marginTop: '5px',
        alignItems: 'center',
        fontSize: 16,
        color: "#000",
        "&$labelFocused": {
          color: "#000"
        }
      },
      labelFocused: {},
      TextField: {
        paddingLeft:'12px',
        borderColor:'#c96',
        borderRadius: 0,
        fontFamily: 'Barlow',
        fontSize: 24,
        fontWeight:500,
        
    },
  
}))



export const CustomTextField=(props)=> {
    const classes = useStyles();
    const {name , variant, color, disabled, onChange, type ,placeHolder,multiline, fullWidth, label, value, ...otherProps } =props
    return (

    <>
    <span className={classes.label}>{label}</span>
        <TextField 
            style={{backgroundColor: '#eee',padding:5, height:'40px',marginLeft:10}}
            type={type}
            multiline={multiline}
            value={value}
            id={name}
            // label={label}
            name={name}
            variant={variant}
            color={color}
            disabled={disabled}
            onChange={onChange}
            InputLabelProps={{
                classes: {
                  root: classes.labelRoot,
                  focused: classes.labelFocused
                }
              }}
            InputProps={{
                disableUnderline: true,
                className:classes.TextField
            }}
        />
        </>
    )
}
