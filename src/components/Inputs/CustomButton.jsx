import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'


const useStyles = makeStyles((theme) => ({
    Button: {
        border:'1px solid #c96',
        marginBottom: 10,
        marginLeft: 100,
        borderRadius: 0,
        fontWeight: 400,
        fontFamily: 'Barlow',
        fontSize: 20,
        backgroundColor: '#FFF',
        color: '#c96',
        '&:hover': {
            backgroundColor: '#c96',
            color: '#FFF',
        
        },
    }
}))


export  const CustomButton=(props)=> {
    const classes = useStyles();
    const {name , variant, color, fullWidth , disabled,onClick, size, label, ...otherProps} =props
    return (
        <Button
            fullWidth={fullWidth}
            id={name}
            name={name}
            variant={variant}
            color={color}
            disabled={disabled}
            onClick={onClick}
            size={size}
            className={classes.Button}
            {...otherProps}
        >
        {label}
        </Button>
    )
}
