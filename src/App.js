import React, { useState } from 'react';
import './App.css';
import Typography from '@material-ui/core/Typography';
import { CustomButton, CustomTextField } from "./components/";
import Grid from '@material-ui/core/Grid';
import { Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { getWeather } from './actions/weather';
import Container from '@material-ui/core/Container';
import Spinner from './components/Spinner';
const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 100,
    padding: 15,
    border: '1px solid #eee',
    flexGrow: 1,
    margin: '1px solid #eee',
  },
  error: {
    marginTop: 10,
    marginBottom: 10
  },
  center: {
    margin: 'auto',
    width: '50%',
    textAlign: 'center',
    padding: '10px'
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

const App = () => {

  const [error, setError] = useState('');
  const [formData, setFormData] = useState({
    city: '',
  });
  const [response, setResponse] = useState({});
  const { city } = formData;
  const onSearch = async () => {

    try {
      const response = await getWeather(formData)

      // <Spinner />
      // as i using promise, the spinner will no be use , also data is captured within time.
      setResponse(response)
    } catch (error) {
      setError('City not found')
    }
  }
  const onChange = e => {
    setError('')
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const getImage = (string) => {
    if (string.includes("cloud")) {
      return <>
        <Typography variant="h3" component="h3">clouds</Typography>
        <img height="100px" alt="Cloud day" src="/img/cloud.png" />
      </>
    }
    if (string.includes("rain")) {
      return <>
        <Typography variant="h3" component="h3">Rain</Typography>
        <img height="100px" alt="Rain day" src="/img/rain.png" />
      </>
    }
    if (string.includes("clear")) {
      return <>
        <Typography variant="h3" component="h3">Clear</Typography>
        <img height="100px" alt="Rain day" src="/img/clear.png" />
      </>
    }
    else {
      <>
        <Typography variant="h3" component="h3">N/A</Typography>
        <img height="100px" alt="Rain day" src="/img/no.png" />
      </>
    }
  }

  const classes = useStyles();
  return (
    <Container width='auto'>
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="h4" component="h4">Today's Weather</Typography>
            <Divider />
          </Grid>
          <Grid item className={classes.error} xs={12}>
            <Typography variant="h6" component="h6" color={error ? "error" : ""} >  {error || response && response.data && response.data.name}</Typography>
          </Grid>

          <CustomTextField
            value={city}
            id="city"
            name="city"
            label="city"
            type="text"
            onChange={onChange}
            fullWidth
          />

          <CustomTextField
            disabled
            value={response && response.data && response.data.sys.country || ''}
            id="country"
            name="country"
            label="country"
            type="text"
            onChange={onChange}
            fullWidth
          />

          <CustomButton
            label="serach"
            onClick={onSearch}
          />

        </Grid>

        {response.data && <>
          <div className={classes.center}>
            {getImage(response.data.weather[0].description)}

            <Typography variant="h5" component="h5">{response.data.weather[0].description}</Typography>
          </div>
          <Typography variant="h5" component="h5">Temperature : {(response.data.main.temp_min - 273.15).toFixed(2)} &#8451;  - {(response.data.main.temp_max - 273.15).toFixed(2)} &#8451;</Typography>
          <Typography variant="h5" component="h5">Humidity :    {response.data.main.humidity + '%'}</Typography>
        </>
        }


      </div>
    </Container>
  );
}

export default App;
